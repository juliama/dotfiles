# dotfiles
My configuration files for Arch Linux, specifically Arch Linux installed on my ThinkPad x200s.
`stow` is used to create symbolic links from the dotfiles directory to where the configs are supposed to be based on folder structure.

## Example usage

    git clone git@gitlab.com:juliama/dotfiles.git .dotfiles
    cd .dotfiles
    make

## Manual config

### oh-my-zsh

Install `oh-my-zsh` using command from `packages/other.packages`.

### vim

1. Install `vim-plug` using command from `packages/other.packages`
2. Run `:PlugInstall` in vim

### wallpaper and i3-lock images
Images are not saved in this repo due to copyright concerns.
Refer to the i3 config to find the used paths.

### polybar

Copy `local.config.example` as `local.config` and fill out interface names.
This info can be obtained using `ip link`.

### automounting deivces

    systemctl start devmon@julia
    systemctl enable devmon@julia

### disabling bluetooth and camera

Bluetooth and the built-in camera can be disabled in BIOS under `Security > I/O Port Access`.
I do this for powersaving purposes.
