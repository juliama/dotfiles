" vim-plug
call plug#begin()
Plug 'itchyny/lightline.vim'
Plug 'raimondi/delimitmate'
Plug 'ntpeters/vim-better-whitespace'
Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'airblade/vim-gitgutter'
Plug 'frazrepo/vim-rainbow'
Plug 'itchyny/vim-gitbranch'
Plug 'yggdroot/indentline'
Plug 'nvie/vim-flake8'
Plug 'Vimjas/vim-python-pep8-indent'
Plug 'chrisbra/Colorizer'
Plug 'ajh17/vimcompletesme'
Plug 'rafi/awesome-vim-colorschemes'
call plug#end()

" plugin settings
set laststatus=2
set noshowmode
let g:lightline = {
      \ 'colorscheme': 'powerline',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'gitbranch#name'
      \ },
      \ }

let g:gitgutter_terminal_reports_focus=0
let g:gitgutter_realtime=1
let g:gitgutter_eager=1
set updatetime=250

let g:rainbow_active=1

let g:indentLine_char_list = ['|', '¦', '┆', '┊']

nmap <F6> :NERDTreeToggleVCS<CR>

" colorscheme gruvbox
" add these two lines at end of color scheme
" doesn't work here in the config :(
" highlight Normal ctermbg=None guibg=None
" highlight NonText ctermbg=None guibg=None

" other
set number
set relativenumber
set ruler
highlight CursorLineNr cterm=none
filetype plugin indent on
syntax on
set showmatch
set ts=4
set sts=4
set sw=4
set autoindent
set expandtab
set backspace=indent,eol,start
highlight ColorColumn ctermbg=magenta
call matchadd('ColorColumn', '\%81v', 80)
