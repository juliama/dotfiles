set number
set relativenumber
set ruler
highlight CursorLineNr cterm=none

filetype plugin on
syntax on
