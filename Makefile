STOW_SYMLINKS += i3-gaps
STOW_SYMLINKS += picom
STOW_SYMLINKS += polybar
STOW_SYMLINKS += redshift
STOW_SYMLINKS += rofi
STOW_SYMLINKS += vim
STOW_SYMLINKS += x11
STOW_SYMLINKS += zsh

all: $(STOW_SYMLINKS)

# create symlinks for all dotfiles with stow
$(STOW_SYMLINKS): %:
	stow $*

clean:
	stow -D $(STOW_SYMLINKS)

.PHONY: all clean $(STOW_SYMLINKS)
